# gc635

Control of a Stanford Research Systems CG635 clock generator

In order to install the correct udev, please check the serial number of the relevant USB<->RS232 converter using something like:
```
udevadm info --attribute-walk --path=/sys/bus/usb-serial/devices/ttyUSB999 | grep serial
```
and then edit the rules file correspondingly.

