#!/usr/bin/env python3

import serial

# From the manual
# The communication parameters are fixed at: 9600 Baud, 8 Data
# bits, 1 Stop bit, No Parity, RTS/CTS Hardware Flow Control.
ser = serial.Serial("/dev/gc635", 9600, timeout=1,
   parity=serial.PARITY_NONE, rtscts=1,
   bytesize=serial.EIGHTBITS, stopbits=serial.STOPBITS_ONE)

def answer(myData):
  print("Answer: " + myData.decode('utf-8'))

def setFreq(f_hz):
  myString = b"FREQ%f\n" % f_hz
  ser.write(myString)

def getFreq():
  ser.write(b'FREQ?\n')
  data=ser.read(100)
  return float(data.decode('utf-8'))

def printVolt():
  ser.write(b"CMOS?0\n")
  v_lo=float(ser.read(100).decode('utf-8'))
  ser.write(b"CMOS?1\n")
  v_hi=float(ser.read(100).decode('utf-8'))
  print("V_LO = %f\tV_HI = %f" % (v_lo, v_hi) )

def setVolt(voltage):
  ser.write(b"CMOS 0, 0.00\n")
  ser.write(b"CMOS 1, %f\n" % voltage)

# optional: check the ID of the instrument
def getModel():
  ser.write(b'*IDN?\n')
  answer(ser.read(100))

