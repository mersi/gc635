#!/usr/bin/env python3

from gcFunctions import *

# Prints the device model
getModel()

# Sets the high level in volts
setVolt(3.50)
printVolt()

# Sets the frequency in Hz
setFreq(40e6+.123456)
print("f = %f Hz" % getFreq())

