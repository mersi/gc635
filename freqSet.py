#!/usr/bin/env python3

from gcFunctions import *

# Sets the frequency in Hz
setFreq(40e6+.123456)
print("f = %f Hz" % getFreq())

